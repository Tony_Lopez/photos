<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title><?= $page_title; ?></title>
    <link rel="stylesheet" href="<?= base_url("css/styles.css") ?>" />
</head>

<body>
    <div id="conteneur">
        <header>
            <h1>Concours photographique</h1>
        </header>

        <nav>
            <ul>
                <li><a href="<?= base_url("Caccueil") ?>">Concours photographique</a></li>
                <li><a href="<?php echo base_url("Ccompetition"); ?>">Les compétitions</a></li>
                <li><a href="<?= base_url("Cphoto") ?>">Galerie photo</a></li>
            </ul>
        </nav>
        <section>

            <?php echo $contenu; ?>

        </section>

        <footer>
            <p>Copyright PHP - Tous droits réservés -
                <a href="#">Contact</a>
            </p>
        </footer>
    </div>
</body>

</html>