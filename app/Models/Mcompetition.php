<?php

namespace App\Models;

use CodeIgniter\Model;

class Mcompetition extends Model
{
    protected $table = 'competition';
    protected $primaryKey = 'ID';
    protected $returnType = 'array';


    public function select_all()
    {
        $requete = $this->select('*');
        return $requete->findAll();
    }

    public function select_detail_by_id($prmId)
    {
        $requete = $this->select('*')->where(['ID' => $prmId]);
        return $requete->findall();
    }
}
