<?php

namespace App\Models;

use CodeIgniter\Model;

class Mphoto extends Model
{
    protected $table = 'photo';
    protected $primaryKey = 'ID';
    protected $returnType = 'array';

    public function select_all_by_id_competition($prmidCompetition)
    {
        return
            $this->select('Titre, Classement, concurrent.Nom, Prenom, Pays, photo.ID')
            ->join('concurrent', 'photo.concurrentID = concurrent.ID', 'left')
            ->where(['competitionID' => $prmidCompetition])
            ->orderby('Classement', 'asc')
            ->findAll();
    }

    public function getAllByIdCompet($prmIdCompetition)
    {
        $requete = $this->select('photo.ID, Titre, Prenom, Nom, Pays, Classement')
            ->join('concurrent', 'photo.concurrentID = concurrent.ID', 'left')
            ->where(['photo.competitionID' => $prmIdCompetition])
            ->orderBy('Classement', 'ASC');
        return $requete->findAll();
    }
}
