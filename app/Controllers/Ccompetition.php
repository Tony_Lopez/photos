<?php

namespace App\Controllers;

use App\Models\Mphoto;
use App\Models\Mcompetition;
use CodeIgniter\Controller;
use \CodeIgniter\Exceptions\PageNotFoundException;

class Ccompetition extends Controller
{
    public function index()
    {
        $model = new Mcompetition();
        $data['result'] = $model->select_All();

        $data['page_title'] = "La liste des compétitions";
        $data['titre1'] = "La liste des compétitions";


        $page['contenu'] = view('competition/v_liste_competition', $data);
        return view('commun/v_template', $page);
    }

    public function detail($prmidCompetition = null)
    {
        if ($prmidCompetition != null) {
            $model = new MCompetition();
            $data['result'] = $model->select_detail_by_id($prmidCompetition);
            if (($data['result']) != 0) {
                $modelphoto = new Mphoto();
                $data['resultphoto'] = $modelphoto->select_all_by_id_competition($prmidCompetition);
                $data['page_title'] = "La compétition nature";
                $data['titre1'] = "La compétition" . $prmidCompetition;

                $page['contenu'] = view('competition/v_detail_competition', $data);
                return view('Commun/v_template', $page);
            } else {
                throw PageNotFoundException::forPageNotFound("Cette compétition n'existe pas !");
            }
        } else {
            throw PageNotFoundException::forPageNotFound("Il faut choisir une compétition !");
        }
    }
}
