<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class Caccueil extends Controller
{
    public function index()
    {
        $data['page_title'] = "Concours Photographique";
        $data['titre1'] = "Les résultats des concours sont publiés ";

        $page['contenu'] = view('v_accueil', $data);

        return view('Commun/v_template', $page);
    }
}
