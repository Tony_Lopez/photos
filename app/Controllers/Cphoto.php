<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Mphoto;
use \CodeIgniter\Exceptions\PageNotFoundException;

class Cphoto extends Controller
{
    public function detail($prmId = null)
    {
        if ($prmId != null) {

            $modelPhoto = new Mphoto();
            $data['resultPhoto'] = $modelPhoto->select_detail_by_id($prmId);
            if (count($data['resultPhoto']) != 0) {

                $data['page_title'] = $data['resultPhoto'][0]['Titre'];

                $page['contenu'] = view('photos/v_detail_photo', $data);
                return view('Commun/v_template', $page);
            } else {
                throw PageNotFoundException::forPageNotFound("Cette compétition n'existe pas !");
            }
        } else {
            throw PageNotFoundException::forPageNotFound("Il faut choisir une compétition !");
        }
    }
}
